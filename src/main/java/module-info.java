module at.hakimst.apr.taskmanager {
    requires javafx.controls;
    requires javafx.fxml;


    opens at.hakimst.apr.taskmanager to javafx.fxml;
    exports at.hakimst.apr.taskmanager;
}