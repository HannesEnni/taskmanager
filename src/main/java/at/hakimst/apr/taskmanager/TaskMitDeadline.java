package at.hakimst.apr.taskmanager;

import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

public class TaskMitDeadline extends Tasks{

    private GregorianCalendar deadline;

    public TaskMitDeadline(String tasktext, GregorianCalendar taskErstellung,  GregorianCalendar deadline) {

        super(tasktext, taskErstellung);
        this.deadline = deadline;
    }


    public GregorianCalendar getDeadline(){
        return deadline;
    }
    public void setDeadline(GregorianCalendar deadline) {
        this.deadline = deadline;
    }
    public boolean istUeberfaellig() {
        GregorianCalendar datumJetzt = new GregorianCalendar();
        GregorianCalendar deadlineDatum = getDeadline();
        return deadlineDatum.before(datumJetzt);
    }

    public boolean istHeuteFaellig() {
        GregorianCalendar datumJetzt = new GregorianCalendar();
        GregorianCalendar deadlineDatum = getDeadline();
        return datumJetzt.get(GregorianCalendar.YEAR) == deadlineDatum.get(GregorianCalendar.YEAR) &&
                datumJetzt.get(GregorianCalendar.MONTH) == deadlineDatum.get(GregorianCalendar.MONTH) &&
                datumJetzt.get(GregorianCalendar.DAY_OF_MONTH) == deadlineDatum.get(GregorianCalendar.DAY_OF_MONTH);
    }


    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String erstellungsdatumStr = dateFormat.format(getTaskErstellung().getTime());
        String deadlineStr = dateFormat.format(getDeadline().getTime());

        return super.toString() + " Erstellungsdatum: " + erstellungsdatumStr + " Deadline: " + deadlineStr;
    }
}