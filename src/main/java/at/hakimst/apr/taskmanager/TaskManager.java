package at.hakimst.apr.taskmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class TaskManager {

    private List<Tasks> tasksList;

    public TaskManager() {

        tasksList = new ArrayList<>();
    }
    public void addTasks(Tasks tasks) {
        tasksList.add(tasks);
    }
    public void alleAufgabenAnzeigen(){
        for (Tasks t : tasksList){
            System.out.println(t.toString());
        }
    }

    public void aufgabenDurchsuchen(String begriff){
        for (Tasks t : tasksList){
            if (t.toString().contains(begriff)){
                System.out.println(t);
            }
        }
    }
    public void deadlinesAnzeigen(){
        for (Tasks t : tasksList){
            if (t instanceof TaskMitDeadline){
                TaskMitDeadline taskMitDeadline = (TaskMitDeadline) t;

                if(taskMitDeadline.istHeuteFaellig()){
                    System.out.println("Heute fällige Aufgaben " + t.getTasktext());
                }
                else if (taskMitDeadline.istUeberfaellig()){
                    System.out.println("Überfällige Aufgaben " + t.getTasktext());
                }
                else {
                    System.out.println("Noch fällige Aufgaben: " + t.toString());
                }
            }
        }
    }

    public void aufgabeErledigt(int taskIndex) {
        if (taskIndex >= 1 && taskIndex <= tasksList.size()) {
            Tasks deletedTask = tasksList.remove(taskIndex - 1);
            System.out.println("Aufgabe wurde gelöscht:\n" + deletedTask);
        } else {
            System.out.println("Ungültige Eingabe!");
        }
    }

    public void fehler() {
        System.out.println("Ungültige Eingabe");
    }
}