package at.hakimst.apr.taskmanager;

import java.util.Scanner;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

public class Kommandozeilenmenü {

    private Scanner scan;
    private TaskManager taskManager;

    public Kommandozeilenmenü() {
        this.scan = new Scanner(System.in);
        this.taskManager = new TaskManager();
    }

    public void menüAnzeigen() {
        System.out.println("******** TASK MANAGER *******");
        System.out.println("1) Alle Aufgaben anzeigen");
        System.out.println("2) Aufgaben durchsuchen");
        System.out.println("3) Deadlines anzeigen");
        System.out.println("4) Neue Aufgabe hinzufügen");
        System.out.println("   a) Standard-Aufgabe");
        System.out.println("   b) Aufgabe mit Deadline (incl. Benachrichtigung)");
        System.out.println("5) Aufgabe erledigt");
        System.out.println("x) Beenden");
        System.out.println("**********************************");
    }
    // ...

    public void start() {
        String choice;
        do {
            menüAnzeigen();
            choice = scan.nextLine();
            switch (choice) {
                case "1":
                    taskManager.alleAufgabenAnzeigen();
                    break;
                case "2":
                    System.out.println("Geben Sie die gewünschte Aufgabe ein");
                    taskManager.aufgabenDurchsuchen(scan.nextLine());
                    break;
                case "3":
                    taskManager.deadlinesAnzeigen();
                    break;
                case "4":
                    System.out.println("Möchten Sie eine Standard-Aufgabe (a) oder eine Aufgabe mit Deadline (b) hinzufügen?");
                    String aufgabenTyp = scan.nextLine();

                    if (aufgabenTyp.equals("a")) {
                        System.out.println("Bitte geben Sie den Text der Standard-Aufgabe ein:");
                        String aufgabenText = scan.nextLine();
                        Tasks neueAufgabe = new Tasks(aufgabenText, new GregorianCalendar());
                        taskManager.addTasks(neueAufgabe);
                        System.out.println("Neue Standard-Aufgabe hinzugefügt: " + neueAufgabe.toString());
                    } else if (aufgabenTyp.equals("b")) {
                        System.out.println("Bitte geben Sie den Text der Aufgabe mit Deadline ein:");
                        String aufgabenText = scan.nextLine();
                        System.out.println("Bitte geben Sie das Deadline-Datum im Format yyyy-MM-dd ein:");
                        String deadlineDatumString = scan.nextLine();

                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date deadlineDate = dateFormat.parse(deadlineDatumString);

                            GregorianCalendar deadlineDatum = new GregorianCalendar();
                            deadlineDatum.setTime(deadlineDate);

                            TaskMitDeadline neueAufgabeMitDeadline = new TaskMitDeadline(aufgabenText, new GregorianCalendar(), deadlineDatum);
                            taskManager.addTasks(neueAufgabeMitDeadline);
                            System.out.println("Neue Aufgabe mit Deadline hinzugefügt: " + neueAufgabeMitDeadline.toString());
                        } catch (ParseException e) {
                            System.out.println("Ungültiges Datumsformat! Bitte verwenden Sie das Format yyyy-MM-dd.");
                        }
                    } else {
                        taskManager.fehler();
                    }
                    break;
                case "5":
                    System.out.println("Geben Sie die Nummer der Aufgabe an");
                    taskManager.aufgabeErledigt(Integer.parseInt(scan.nextLine()));
                    break;
                case "x":
                    System.out.println("Programm wird beendet.");
                    break;
                default:
                    taskManager.fehler();
                    break;
            }
        } while (!choice.equals("x"));
    }
}