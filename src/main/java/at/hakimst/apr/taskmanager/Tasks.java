package at.hakimst.apr.taskmanager;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Tasks {
    private String tasktext;
    private GregorianCalendar taskErstellung;
    private boolean erledigt;

    public Tasks(String tasktext, GregorianCalendar taskErstellung) {
        this.tasktext = tasktext;
        this.taskErstellung = taskErstellung;
    }


    public boolean isErledigt() {
        return erledigt;
    }

    public void setErledigt(boolean erledigt) {
        this.erledigt = erledigt;
    }

    public String getTasktext() {
        return tasktext;
    }

    public GregorianCalendar getTaskErstellung() {

        return taskErstellung;
    }

    public void setTasktext(String tasktext) {

        this.tasktext = tasktext;
    }

    public void setTaskErstellung(GregorianCalendar taskErstellung) {

        this.taskErstellung = taskErstellung;
    }


    public String toString() {
        return "Task: " + getTasktext() + " Erstellungsdatum: " + getTaskErstellung() + " Erledigt: " + isErledigt();
    }
}
